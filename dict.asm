%include "lib.inc"
global find_word
%define size 8

section .text
find_word:
	mov r8, rdi
	mov r9, rsi
	.loop:
		add r9, size
		mov rsi, r9
		mov rdi, r8
		push r8
		push r9
		push rdi
		push rsi
		call string_equals
		pop rsi
		pop rdi
		pop r9
		pop r8
		cmp rax, 1
		je .found
		mov r9, [r9 - size]
		cmp r9, 0
		je .not_found
		jmp .loop
	.found:
		sub r9, size
		mov rax, r9
		ret
	.not_found:
		xor rax, rax
		ret
