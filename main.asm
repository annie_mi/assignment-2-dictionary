%include "colon.inc"
%include "words.inc"
%include "lib.inc"
%include "dict.inc"
%define buffer_size 255
global _start

section .data

error_buffer_overflow:
db "Buffer overflow", `\n`, 0
error_no_key:
db "There is no such key", `\n`, 0
buffer:
db 256 dup(0)

section .text

_start:
    xor rax, rax
    mov rdi, buffer
    mov rsi, buffer_size
    call read_word
    cmp rax, 0
    je .error_buffer
    mov rdi, rax
    mov rsi, NEXT_ELEMENT
    call find_word
    cmp rax, 0
    je .error_key
    add rax, 8
    mov rdi, rax
    push rdi
    call string_length
    pop rdi
    lea rdi, [rdi+rax+1]
    call print_string
    call print_newline
    mov rdi, 0
    call exit
.error_buffer:
    mov rdi, error_buffer_overflow
    call print_stderr
    mov rdi, 1
    call exit
.error_key:
    mov rdi, error_no_key
    call print_stderr
    mov rdi, 1
    call exit
