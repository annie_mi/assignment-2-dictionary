%define NEXT_ELEMENT 0

%macro colon 2
%ifid %2
%2:
dq NEXT_ELEMENT

%define NEXT_ELEMENT %2
%else
    %error " Не указатель"
%endif
%ifstr %1
db %1, 0
%else
    %error "Должно быть строкой"
%endif
%endmacro