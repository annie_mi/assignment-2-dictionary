%.o: %.asm
	nasm -f elf64 $^

main: main.o dict.o lib.o dict.inc lib.inc colon.inc words.inc
	ld main.o dict.o lib.o -o main

.PHONY: clean
clean:
	rm -f *.o